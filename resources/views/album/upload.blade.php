<!DOCTYPE html>
<html lang="en">

<head>
    @if(Auth::user()->pro == 'yes')
        @include('layouts.pro.head')
        <link rel="stylesheet" href="{{ asset('css/dropzone.css') }}" />
        <script src="{{ asset('js/dropzone.js') }}"></script>

        <link rel="stylesheet" href="https://www.shotoindia.com/css/lightbox.min.css"/>
    @else
        @include('layouts.head')
    @endif
</head>
<style>
    .form-group input[type=file] {
        opacity: unset;
        position: inherit;
    }

    .image-frame{
        margin: 15px 0;
    }

    .img-thumbnail{
        border: 0px;
    }
</style>
<body class="sidebar-mini">
<div class="wrapper ">
    @if(Auth::user()->pro == 'yes')
        @include('layouts.pro.side-nav')
    @else
        @include('layouts.side-nav')
    @endif
    <div class="main-panel">
        <!-- Navbar -->
    @if(Auth::user()->pro == 'yes')
        @include('layouts.pro.nav')
    @else
        @include('layouts.nav')
    @endif
    <!-- End Navbar -->
        <div class="panel-header panel-header-sm">
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="title">{{ $album_name }}</h5>
                        </div>
                        <div class="container-fluid">
                            <div id="dropzone">
                                <form action="{{ route('image_post') }}" method="post" class="dropzone needsclick dz-clickable" id="demo-upload">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="album_id" value="{{ $album_id }}">
                                    <input type="hidden" name="album_name" value="{{ $album_name }}">
                                    <div class="dz-message needsclick">
                                        Drop files here or click to upload.<br>
                                        <span class="note needsclick">(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</span>
                                    </div>

                                </form>
                            </div>
                            <br>
                            <div class="card-body all-icons">
                                <div class="row">
                                    @foreach($images as $image)
                                        <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 col-xs-6">
                                            <div class="image-frame">
                                                <a href="{{ str_replace('public','storage',asset($image->image_url)) }}" data-lightbox="Class Room">
                                                    <img src="{{ str_replace('public','storage',asset($image->thumbnail_url)) }}" style="width: 100%;" class="img-thumbnail"/>
                                                </a>
                                            </div>

                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footer')
    </div>
</div>
@if(Auth::user()->pro == 'yes')
    @include('layouts.pro.fixed-plugin')
    @include('layouts.pro.script')
    <script src="https://www.shotoindia.com/js/lightbox.min.js"></script>
@else
    @include('layouts.script')
@endif
</body>

</html>