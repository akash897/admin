
<!DOCTYPE html>
<html lang="en">

<head>
    @if(Auth::user()->pro == 'yes')
        @include('layouts.pro.head')
        <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    @else
        @include('layouts.head')
        <script src="{{ asset('js/sweetalert.min.js') }}"></script>
        <link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('js/font-awesome.min.js') }}" />
    @endif
</head>
<body class="sidebar-mini">
<div class="wrapper ">
    @if(Auth::user()->pro == 'yes')
        @include('layouts.pro.side-nav')
    @else
        @include('layouts.side-nav')
    @endif
    <div class="main-panel">
        <!-- Navbar -->
    @if(Auth::user()->pro == 'yes')
        @include('layouts.pro.nav')
    @else
        @include('layouts.nav')
    @endif
    <!-- End Navbar -->
        <div class="panel-header panel-header-sm">
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <span class="h4">Album List</span>
                                <a href="{{ route('album.create') }}">
                                    <span class="btn btn-outline-primary btn-sm float-sm-right">Add New</span>
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" style="width:100%" id="user_table">
                                    <thead class=" text-primary">
                                    <th>
                                        Sr no
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Status
                                    </th>

                                    <th class="text-right">
                                        Action
                                    </th>
                                    </thead>
                                    <tbody>
                                    @foreach($values as $value)
                                        <tr id="row_{{$value->id}}">
                                            <td>
                                                {{ $value->id ? $value->id : 'N/A' }}
                                            </td>
                                            <td>
                                                {{ $value->name ? $value->name : 'N/A' }}
                                            </td>
                                            <td>
                                                {{ $value->deleted_at ? 'Deleted' : 'Active' }}
                                            </td>
                                            <td class="text-right">
                                                <a href="{{ route('album.show',['album'=>$value->key]) }}">
                                                    <button class="btn btn-round btn-primary btn-sm like" href="#"><spna class="fas fa-upload"></spna></button>
                                                </a>
                                                @if($value->deleted_at == null)
                                                    <a href="#">
                                                        <button class="btn btn-round btn-warning btn-sm edit" id="edit_{{$value->id}}"><spna class="fas fa-pencil-alt"></spna></button>
                                                    </a>
                                                    <button class="btn btn-round btn-danger btn-sm edit" onclick="deleteData({{ $value->id }})" id="delete_{{$value->id}}" type="submit" id="delete_{{$value->id}}"><spna class="far fa-trash-alt"></spna></button>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footer')
    </div>
</div>
<!--   Core JS Files --!>
@if(Auth::user()->pro == 'yes')
    @include('layouts.pro.fixed-plugin')
    @include('layouts.pro.script')
@else
    @include('layouts.script')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js"></script>
@endif

<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function deleteData(id){
        var token = '{{ csrf_token() }}';
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "Cancel",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: true,
                },
                confirm: {
                    text: "OK",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                }
            },
            dangerMode: true,
            closeOnEsc: false,
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url : '{{ env('APP_URL') }}'+'/black-belt/' + id ,
                        type: 'POST',
                        data : {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token
                        },
                        success: function(){
                            swal({
                                title: "Success!",
                                text : "Post has been deleted \n Click OK to refresh the page",
                                icon : "success",
                            });
                            document.getElementById("edit_"+id).remove();
                            document.getElementById("delete_"+id).remove();
                        },
                        error : function(){
                            swal({
                                title: 'Opps...',
                                text : data.message,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
                } else {
                    swal("Your imaginary file is safe!");
                }
            });
    }

    ( function($){

        $(document).ready(function() {
            $('#datatable').DataTable();
        } );

        $(document).ready(function() {
            $('#user_table').DataTable();
        } );

    }) (jQuery);

    (function($){
        $(document).ready(function(){

            $('input[type=search]').removeClass('form-control-sm');

        });
    })(jQuery);
</script>
</body>

</html>