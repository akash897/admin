<!DOCType html>
<html>
    <head>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" ></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
    </head>
    <body>
        <div id="dropzone">
            <form action="{{ route('image_post') }}" method="post" class="dropzone needsclick dz-clickable" id="demo-upload">
                {{ csrf_field() }}
                <input type="hidden" name="album" value="{{ $album }}">
                <div class="dz-message needsclick">
                    Drop files here or click to upload.<br>
                    <span class="note needsclick">(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</span>
                </div>

            </form>
        </div>
    </body>
</html>