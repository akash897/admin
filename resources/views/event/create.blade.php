<!DOCTYPE html>
<html lang="en">

<head>
    @if(Auth::user()->pro == 'yes')
        @include('layouts.pro.head')
    @else
        @include('layouts.head')
    @endif
</head>
<style>
    .form-group input[type=file] {
        opacity: unset;
        position: inherit;
    }
</style>
<body class="sidebar-mini">
<div class="wrapper ">
    @if(Auth::user()->pro == 'yes')
        @include('layouts.pro.side-nav')
    @else
        @include('layouts.side-nav')
    @endif
    <div class="main-panel">
        <!-- Navbar -->
    @if(Auth::user()->pro == 'yes')
        @include('layouts.pro.nav')
    @else
        @include('layouts.nav')
    @endif
    <!-- End Navbar -->
        <div class="panel-header panel-header-sm">
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="title">Create new Event</h5>
                        </div>
                        <form class="form-horizontal" method="POST" action="{{ route('event.store') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 pr-1">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input id="name" type="text" class="form-control" name="event_name"  required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 pr-1">
                                        <div class="form-group">
                                            <label>Image</label>
                                            <input id="name" type="file" class="form-control" name="name"  />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="card-body">
                                <center>
                                    <button class="btn btn-primary">Submit</button>
                                </center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footer')
    </div>
</div>
@if(Auth::user()->pro == 'yes')
    @include('layouts.pro.fixed-plugin')
    @include('layouts.pro.script')
@else
    @include('layouts.script')
@endif
</body>

</html>