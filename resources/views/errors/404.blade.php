<?php
    $page_name = 'Page Not Found';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.head')
</head>
<style>
    .form-control {
        border: 0;
    }
    .font-color-blue{
        color: orangered;
    }
</style>
<body class="">
<div class="wrapper ">
    @include('layouts.side-nav')
    <div class="main-panel">
        <!-- Navbar -->
    @include('layouts.nav')
    <!-- End Navbar -->
        <div class="panel-header panel-header-sm">
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="title">Page not found</h5>
                        </div>
{{--                        <div class="card-body"></div>--}}
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12 pr-1">
                                    The link you followed may be broken, or the page may have been removed. The requested URL <span class="font-color-blue">{{ url()->current() }}</span> was not found on this server
                                </div>
                            </div>
                        </div>
                        <div class="card-body"></div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footer')
    </div>
</div>
@include('layouts.script')
</body>

</html>