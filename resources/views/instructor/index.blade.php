
<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.head')
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    <link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('js/font-awesome.min.js') }}" />
</head>
<body class="">
<div class="wrapper ">
    @include('layouts.side-nav')
    <div class="main-panel">
        <!-- Navbar -->
    @include('layouts.nav')
    <!-- End Navbar -->
        <div class="panel-header panel-header-sm">
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Black Belt List</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" style="width:100%" id="user_table">
                                    <thead class=" text-primary">
                                    <th>
                                        Sr no
                                    </th>
                                    <th>
                                        Full Name
                                    </th>
                                    <th>
                                        Dan
                                    </th>
                                    <th>
                                        Year
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                    <th class="text-right">
                                        Action
                                    </th>
                                    </thead>
                                    <tbody>
                                    @foreach($values as $user)
                                        <tr id="row_{{$user->id}}">
                                            <td>
                                                {{ $user->id ? $user->id : 'N/A' }}
                                            </td>
                                            <td>
                                                {{ $user->full_name ? $user->full_name : 'N/A' }}
                                            </td>
                                            <td>
                                                {{ $user->dan_list->dan ? $user->dan_list->dan : 'N/A' }}
                                            </td>
                                            <td>
                                                {{ $user->year ? $user->year : 'N/A' }}
                                            </td>
                                            <td>
                                                {{ $user->deleted_at ? 'Deleted' : 'Active' }}
                                            </td>
                                            <td class="text-right">
                                                <a href="{{ route('instructor.view',['id'=>$user->id]) }}">
                                                    <button class="btn btn-primary" href="#"><spna class="fas fa-eye"></spna></button>
                                                </a>
                                                @if($user->deleted_at == null)
                                                    <a href="{{ route('instructor.edit',['id'=>$user->id]) }}">
                                                        <button class="btn btn-success" id="edit_{{$user->id}}"><spna class="fas fa-pencil-alt"></spna></button>
                                                    </a>
                                                    <button class="btn btn-danger" onclick="deleteData({{ $user->id }})" id="delete_{{$user->id}}" type="submit" id="delete_{{$user->id}}"><spna class="far fa-trash-alt"></spna></button>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footer')
    </div>
</div>
<!--   Core JS Files   -->
<script src="./js/core/jquery.min.js"></script>
<script src="./js/core/popper.min.js"></script>
<script src="./js/core/bootstrap.min.js"></script>
<script src="./js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Chart JS -->
<script src="./js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="./js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="./js/now-ui-dashboard.min.js?v=1.1.0" type="text/javascript"></script>
<!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
<script src="./demo/demo.js"></script>


<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js"></script>

<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function deleteData(id){
        console.log(id);
        var token = '{{ csrf_token() }}';
        console.log(token.toString());
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url : '{{ env('APP_URL') }}'+'/instructor/' + id ,
                        type: 'POST',
                        data : {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token
                        },
                        success: function(){
                            swal({
                                title: "Success!",
                                text : "Post has been deleted \n Click OK to refresh the page",
                                icon : "success",
                            });
                            document.getElementById("edit_"+id).remove();
                            document.getElementById("delete_"+id).remove();
                        },
                        error : function(){
                            swal({
                                title: 'Opps...',
                                text : data.message,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
                } else {
                    swal("Your imaginary file is safe!");
                }
            });
    }

    ( function($){

        $(document).ready(function() {
            $('#user_table').DataTable();
        } );

    }) (jQuery);

    (function($){
        $(document).ready(function(){

            // var search_box = '<div class="input-group no-border">'+
            //                 '<input type="search" value="" class="form-control" placeholder="Search..." aria-controls="user_table">'+
            //                 '<div class="input-group-append">'+
            //                 '<div class="input-group-text">'+
            //                 '<i class="now-ui-icons ui-1_zoom-bold"></i>'+
            //                 '</div>'+
            //                 '</div>'+
            //                 '</div>';
            //
            // $('#user_table_filter').html(search_box);

            $('input[type=search]').removeClass('form-control-sm');
            // $('input[type=search]').attr("placeholder", "Search");


        });
    })(jQuery);
</script>
</body>

</html>