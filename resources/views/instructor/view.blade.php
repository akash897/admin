
<!DOCTYPE html>
<html lang="en">

<head>
    @include('layouts.head')
</head>
<style>
    .form-control {
        border: 0;
    }
</style>
<body class="">
<div class="wrapper ">
    @include('layouts.side-nav')
    <div class="main-panel">
        <!-- Navbar -->
    @include('layouts.nav')
    <!-- End Navbar -->
        <div class="panel-header panel-header-sm">
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="title">View</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Full Name</label>
{{--                                        --}}{{--                                            <input type="label" class="form-control" disabled="" placeholder="Company" value="Creative Code Inc.">--}}
                                        <lable class="form-control">{{ $value->full_name ? $value->full_name : 'N/A' }}</lable>
                                    </div>
                                </div>
                                <div class="col-md-6 px-1">
                                    <div class="form-group">
                                        <label>Dan</label>
                                        <lable class="form-control">{{ $value->dan_list ? $value->dan_list->dan : 'N/A' }}</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Instructor</label>
                                        <lable class="form-control">{{ $value->instructor ? ucfirst($value->instructor) : 'N/A' }}</lable>
                                    </div>
                                </div>
                                <div class="col-md-6 pl-1">
                                    <div class="form-group">
                                        <label>Year</label>
                                        <lable class="form-control">{{ $value->year ? $value->year : 'N/A' }}</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Created By</label>
                                        <lable class="form-control">{{ $value->created_by ? $value->created_by : 'N/A' }}</lable>
                                    </div>
                                </div>
                                <div class="col-md-6 px-1">
                                    <div class="form-group">
                                        <label>Created At</label>
                                        <lable class="form-control">{{ $value->created_at ? $value->created_at : 'N/A' }}</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Modified By</label>
                                        <lable class="form-control">{{ $value->modified_by ? $value->modified_by : 'N/A' }}</lable>
                                    </div>
                                </div>
                                <div class="col-md-6 pl-1">
                                    <div class="form-group">
                                        <label>Updated At</label>
                                        <lable class="form-control">{{ $value->updated_at ? $value->updated_at : 'N/A' }}</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <lable class="form-control label label-success">{{ $value->deleted_at ? 'Deactivated' : 'Active' }}</lable>
                                    </div>
                                </div>
                                @if($value->deleted_at != null)
                                    <div class="col-md-6 pl-1">
                                        <div class="form-group">
                                            <label>Deleted At</label>
                                            <lable class="form-control">{{ $value->deleted_at ? $value->deleted_at : 'N/A' }}</lable>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Training Start</label>
                                        {{--                                        --}}{{--                                            <input type="label" class="form-control" disabled="" placeholder="Company" value="Creative Code Inc.">--}}
                                        <lable class="form-control">{{ $instructor->training_start ? $instructor->training_start : 'N/A' }}</lable>
                                    </div>
                                </div>
                                <div class="col-md-6 px-1">
                                    <div class="form-group">
                                        <label>Teaching Start</label>
                                        <lable class="form-control">{{ $instructor->teaching_start ? $instructor->teaching_start : 'N/A' }}</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Phone Number</label>
                                        <lable class="form-control">{{ $instructor->phone_no ? ucfirst($instructor->phone_no) : 'N/A' }}</lable>
                                    </div>
                                </div>
                                <div class="col-md-6 pl-1">
                                    <div class="form-group">
                                        <label>Address</label>
                                        <lable class="form-control">{{ $instructor->address ? $instructor->address : 'N/A' }}</lable>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-user">
                        <div class="image">
                            <img src="{{ asset('img/bg5.jpg') }}" alt="...">
                        </div>
                        <div class="card-body">
                            <div class="author">
                                <a href="#">
                                    <img class="avatar border-gray" src="{{ !empty($value->image_url) ? asset($value->image_url) : asset('img/mike.jpg') }}" alt="{{$value->full_name}}">
                                    <h5 class="title">{{ $value->full_name ? $value->full_name : ''  }}</h5>
                                </a>
                                <p class="description">
                                    {{ $value->dan_list->dan ? $value->dan_list->dan : '' }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footer')
    </div>
</div>
@include('layouts.script')
</body>

</html>