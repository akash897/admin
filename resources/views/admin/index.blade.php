<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    @if(Auth::user()->pro == 'yes')
        @include('layouts.pro.head')
    @else
        @include('layouts.head')
    @endif
</head>
<style>
    .form-control{
        border: 0;
    }
</style>
@if(Auth::user()->pro == 'yes')
    <body class="{{ Auth::user()->user_setting->sidebar_mini == 'on' ? ' sidebar-mini ' : '' }}">
@else
    <body>
@endif
<div class="wrapper ">
    @if(Auth::user()->pro == 'yes')
        @include('layouts.pro.side-nav')
    @else
        @include('layouts.side-nav')
    @endif


    <div class="main-panel" id="main-panel">
        <!-- Navbar -->
        @if(Auth::user()->pro == 'yes')
            @include('layouts.nav')
        @else
            @include('layouts.nav')
        @endif
        <!-- End Navbar -->

        <div class="panel-header panel-header-sm"></div>
        <div class="content">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="title">Profile</h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-5 pr-1">
                                    <div class="form-group">
                                        <label>Name</label>
                                        {{--                                            <input type="label" class="form-control" disabled="" placeholder="Company" value="Creative Code Inc.">--}}
                                        <lable class="form-control">{{ $user->name ? $user->name : 'N/A' }}</lable>
                                    </div>
                                </div>
                                <div class="col-md-3 px-1">
                                    <div class="form-group">
                                        <label>Role</label>
                                        <lable class="form-control">{{ $user->role->name ? $user->role->name : 'N/A' }}</lable>
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <lable class="form-control">{{ $user->email ? $user->email : 'N/A' }}</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        <lable class="form-control">{{ $user->name ? $user->name : 'N/A' }}</lable>
                                    </div>
                                </div>
                                <div class="col-md-6 pl-1">
                                    <div class="form-group">
                                        <label>Phone No.</label>
                                        <lable class="form-control">{{ $user->phone_no ? $user->phone_no : 'N/A' }}</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label>Created By</label>
                                        <lable class="form-control">{{ $user->created_by ? $user->created_by : 'N/A' }}</lable>
                                    </div>
                                </div>
                                <div class="col-md-4 px-1">
                                    <div class="form-group">
                                        <label>Modified By</label>
                                        <lable class="form-control">{{ $user->modified_by ? $user->modified_by : 'N/A' }}</lable>
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>Account Status</label>
                                        <lable class="form-control label label-success">{{ $user->deleted_at ? 'Deactivated' : 'Active' }}</lable>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 pr-1">
                                    <div class="form-group">
                                        <label>Created At</label>
                                        <lable class="form-control">{{ $user->created_at ? $user->created_at : 'N/A' }}</lable>
                                    </div>
                                </div>
                                <div class="col-md-4 px-1">
                                    <div class="form-group">
                                        <label>Updated At</label>
                                        <lable class="form-control">{{ $user->updated_at ? $user->updated_at : 'N/A' }}</lable>
                                    </div>
                                </div>
                                @if($user->deleted_at != null)
                                    <div class="col-md-4 pl-1">
                                        <div class="form-group">
                                            <label>Deleted At</label>
                                            <lable class="form-control">{{ $user->deleted_at ? $user->deleted_at : 'N/A' }}</lable>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-user">
                        <div class="image">
                            <img src="./img/bg5.jpg" alt="...">
                        </div>
                        <div class="card-body">
                            <div class="author">
                                <a href="#">
                                    <img class="avatar border-gray" src="./img/mike.jpg" alt="...">
                                    <h5 class="title">{{Auth::user()->name}}</h5>
                                </a>
                                <p class="description">
                                    {{Auth::user()->email}}
                                </p>
                            </div>
                            <p class="description text-center">
                                {{Auth::user()->role->name}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footer')


    </div>

</div>
@if(Auth::user()->pro == 'yes')
    @include('layouts.pro.fixed-plugin')
    @include('layouts.pro.script')
@else
    @include('layouts.script')
@endif
</body>
</html>
