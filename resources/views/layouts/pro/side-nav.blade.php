<div class="sidebar" data-color="{{ Auth::user()->user_setting->color_name }}">
    <!--
        Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
    -->

    <div class="logo">
        <a href="{{ env('APP_URL') }}" class="simple-text logo-mini">
            SA
        </a>
        <a href="{{ env('APP_URL') }}" class="simple-text logo-normal">
            Shotokan Admin
        </a>

        <div class="navbar-minimize">
            <button id="minimizeSidebar" class="btn btn-simple btn-icon btn-neutral btn-round">
                <i class="now-ui-icons text_align-center visible-on-sidebar-regular"></i>
                <i class="now-ui-icons design_bullet-list-67 visible-on-sidebar-mini"></i>
            </button>
        </div>

    </div>

    <div class="sidebar-wrapper" id="sidebar-wrapper">

        <div class="user">
            <div class="photo">
                <img src="{{ asset('pro/img/james.jpg') }}" />
            </div>
            <div class="info">
                <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                    <span>
                        {{ Auth::user()->name }}
                        <b class="caret"></b>
                    </span>
                </a>
                <div class="clearfix"></div>
                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        <li {{ $page_name == 'User Profile' ? 'active' : '' }} >
                            <a href="{{ route('profile') }}">
                                <span class="sidebar-mini-icon">MP</span>
                                <span class="sidebar-normal">My Profile</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="sidebar-mini-icon">EP</span>
                                <span class="sidebar-normal">Edit Profile</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="sidebar-mini-icon">S</span>
                                <span class="sidebar-normal">Settings</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <ul class="nav">
            <li class="{{ $page_name== 'User List' ? 'active' : ''  }}">
                <a href="{{ route('user.index') }}">
                    <i class="now-ui-icons business_badge"></i>
                    <p>Users</p>
                </a>
            </li>
            <li >
                <a data-toggle="collapse" href="#pagesExamples" >
                    <i class="now-ui-icons business_badge"></i>
                    <p>
                        Black Belt <b class="caret"></b>
                    </p>
                </a>

                <div class="collapse " id="pagesExamples">
                    <ul class="nav">

                        <li class="{{ $page_name== 'Black Belt List' ? 'active' : ''  }}">
                            <a href="{{ route('black-belt.index') }}">
                                <span class="sidebar-mini-icon">L</span>
                                <span class="sidebar-normal"> List </span>
                            </a>
                        </li>

                        <li class="{{ $page_name== 'Create' ? 'active' : ''  }}">
                            <a href="{{ route('black-belt.create') }}">
                                <span class="sidebar-mini-icon">CN</span>
                                <span class="sidebar-normal"> Create New </span>
                            </a>
                        </li>

                    </ul>
                </div>
            </li>

            <li >
                <a data-toggle="collapse" href="#instructorsExamples" >
                    <i class="now-ui-icons business_badge"></i>
                    <p>
                        Instructor <b class="caret"></b>
                    </p>
                </a>

                <div class="collapse " id="instructorsExamples">
                    <ul class="nav">

                        <li class="{{ $page_name== 'Instructor List' ? 'active' : ''  }}">
                            <a href="{{ route('instructor.index') }}">
                                <span class="sidebar-mini-icon">L</span>
                                <span class="sidebar-normal"> List </span>
                            </a>
                        </li>

{{--                        <li class="{{ $page_name== 'Create' ? 'active' : ''  }}">--}}
{{--                            <a href="{{ route('instructor.create') }}">--}}
{{--                                <span class="sidebar-mini-icon">CN</span>--}}
{{--                                <span class="sidebar-normal"> Create New </span>--}}
{{--                            </a>--}}
{{--                        </li>--}}

                    </ul>
                </div>
            </li>

            <li >


                <a data-toggle="collapse" href="#eventsExamples" >

                    <i class="now-ui-icons education_atom"></i>

                    <p>
                        Events <b class="caret"></b>
                    </p>
                </a>

                <div class="collapse " id="eventsExamples">
                    <ul class="nav">

                        <li class="{{ $page_name== 'Event' ? 'active' : ''  }}">
                            <a href="{{ route('event.index') }}">
                                <span class="sidebar-mini-icon">L</span>
                                <span class="sidebar-normal">Event List</span>
                            </a>
                        </li>

                        <li class="{{ $page_name== 'Create Event' ? 'active' : ''  }}">
                            <a href="{{ route('event.create') }}">
                                <span class="sidebar-mini-icon">C</span>
                                <span class="sidebar-normal">Create List</span>
                            </a>
                        </li>

                    </ul>
                </div>


            </li>

            <li >


                <a data-toggle="collapse" href="#albumExamples" >

                    <i class="now-ui-icons education_atom"></i>

                    <p>
                        Album <b class="caret"></b>
                    </p>
                </a>

                <div class="collapse " id="albumExamples">
                    <ul class="nav">

                        <li class="{{ $page_name== 'Album' ? 'active' : ''  }}">
                            <a href="{{ route('album.index') }}">
                                <span class="sidebar-mini-icon">L</span>
                                <span class="sidebar-normal">Album List</span>
                            </a>
                        </li>

                        <li class="{{ $page_name== 'Create Album' ? 'active' : ''  }}">
                            <a href="{{ route('album.create') }}">
                                <span class="sidebar-mini-icon">C</span>
                                <span class="sidebar-normal">Create List</span>
                            </a>
                        </li>

                    </ul>
                </div>


            </li>


        </ul>
    </div>
</div>