<div class="fixed-plugin">
    <div class="dropdown show-dropdown">
        <a href="#" data-toggle="dropdown">
            <i class="fa fa-cog fa-2x"> </i>
        </a>
        <ul class="dropdown-menu">
            <li class="header-title"> Sidebar Background</li>
            <li class="adjustments-line">
                <a href="javascript:void(0)" class="switch-trigger background-color">
                    <div class="badge-colors text-center">
                        <span class="badge filter badge-yellow {{ Auth::user()->user_setting->color_name == 'yellow' ? 'active' : '' }}" data-color="yellow"></span>
                        <span class="badge filter badge-blue {{ Auth::user()->user_setting->color_name == 'blue' ? 'active' : '' }}" data-color="blue"></span>
                        <span class="badge filter badge-green {{ Auth::user()->user_setting->color_name == 'green' ? 'active' : '' }}" data-color="green"></span>
                        <span class="badge filter badge-orange {{ Auth::user()->user_setting->color_name == 'orange' ? 'active' : '' }}" data-color="orange"></span>
                        <span class="badge filter badge-red {{ Auth::user()->user_setting->color_name == 'red' ? 'active' : '' }}" data-color="red"></span>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </li>


            <li class="header-title">
                Sidebar Mini
            </li>
            <li class="adjustments-line">

                <div class="togglebutton switch-sidebar-mini">
                    <span class="label-switch">OFF</span>
                    <input type="checkbox" name="checkbox" checked class="bootstrap-switch"
                           data-on-label=""
                           data-off-label=""
                    />
                    <span class="label-switch label-right">ON</span>
                </div>
            </li>
            <li class="clearfix"></li>
        </ul>
    </div>
</div>