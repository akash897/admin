<meta charset="utf-8" />
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('pro/img/apple-icon.png') }}">
<link rel="icon" type="image/png" href="{{ asset('pro/img/favicon.png') }}">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>
    Now UI Dashboard PRO  by Creative Tim
</title>

<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />


<!-- Extra details for Live View on GitHub Pages -->
<!-- Canonical SEO -->
<link rel="canonical" href="https://admin.shotoindia.com" />


<!--  Social tags      -->
<meta name="keywords" content="Admin, Shotoindia">
<meta name="description" content="Can use admin database of shotoindia.com website">




<!--     Fonts and icons     -->

<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />

<link rel="stylesheet" href="{{ asset('pro/css/fontawesome.css') }}">

<!-- CSS Files -->

<link href="{{ asset('pro/css/bootstrap.min.css') }}" rel="stylesheet" />




<link href="{{ asset('pro/css/now-ui-dashboard.min290d.css?v=1.4.1') }}" rel="stylesheet" />





<!-- CSS Just for demo purpose, don't include it in your project -->
<link href="{{ asset('pro/demo/demo.css') }}" rel="stylesheet" />