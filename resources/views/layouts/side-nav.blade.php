<div class="sidebar" data-color="orange">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="blue | green | orange | red | yellow"
  -->
    <div class="logo">
        <a href="{{ env('APP_URL') }}" class="simple-text logo-mini">
            SA
        </a>
        <a href="{{ env('APP_URL') }}" class="simple-text logo-normal">
            Shotokan Admin
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
{{--            <li>--}}
{{--                <a href="./dashboard.html">--}}
{{--                    <i class="now-ui-icons design_app"></i>--}}
{{--                    <p>Dashboard</p>--}}
{{--                </a>--}}
{{--            </li>--}}

{{--            <li>--}}
{{--                <a href="./map.html">--}}
{{--                    <i class="now-ui-icons location_map-big"></i>--}}
{{--                    <p>Maps</p>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li class="{{ $page_name == '' ? 'active' : '' }}">--}}
{{--                <a href="./notifications.html">--}}
{{--                    <i class="now-ui-icons ui-1_bell-53"></i>--}}
{{--                    <p>Notifications</p>--}}
{{--                </a>--}}
{{--            </li>--}}
            <li class="{{ $page_name == 'User Profile' ? 'active' : '' }} ">
                <a href="{{ route('profile') }}">
                    <i class="now-ui-icons users_single-02"></i>
                    <p>User Profile</p>
                </a>
            </li>
            <li class="{{ $page_name== 'User List' ? 'active' : ''  }}">
                <a href="{{ route('user.index') }}">
                    <i class="now-ui-icons business_badge"></i>
                    <p>Users</p>
                </a>
            </li>
            <li class="{{ $page_name== 'Black Belt List' ? 'active' : ''  }}">
                <a href="{{ route('black-belt.index') }}">
                    <i class="now-ui-icons business_badge"></i>
                    <p>Black Belts</p>
                </a>
            </li>
            <li class="{{ $page_name== 'Instructor List' ? 'active' : ''  }}">
                <a href="{{ route('instructor.index') }}">
                    <i class="now-ui-icons business_badge"></i>
                    <p>Instructor</p>
                </a>
            </li>
{{--            <li>--}}
{{--                <a href="./tables.html">--}}
{{--                    <i class="now-ui-icons design_bullet-list-67"></i>--}}
{{--                    <p>Table List</p>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="./typography.html">--}}
{{--                    <i class="now-ui-icons text_caps-small"></i>--}}
{{--                    <p>Typography</p>--}}
{{--                </a>--}}
{{--            </li>--}}
        </ul>
    </div>
</div>