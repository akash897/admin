
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="./img/apple-icon.png">
    <link rel="icon" type="image/png" href="./img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        Now UI Dashboard by Creative Tim
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <!-- CSS Files -->
    <link href="./css/bootstrap.min.css" rel="stylesheet" />
    <link href="./css/now-ui-dashboard.css?v=1.1.0" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="./demo/demo.css" rel="stylesheet" />


    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" />
</head>
<body class="">
<div class="wrapper ">
    @include('layouts.side-nav')
    <div class="main-panel">
        <!-- Navbar -->
    @include('layouts.nav')
    <!-- End Navbar -->
        <div class="panel-header panel-header-sm">
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"> User Table</h4>
                        </div>
                        <div class="card-body">
                            <div class="">
                                <table class="table" style="width:100%" id="user_table">
                                    <thead class=" text-primary">
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Phone No
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                        <th class="text-right">
                                            Action
                                        </th>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td>
                                                    {{ $user->name ? $user->name : 'N/A' }}
                                                </td>
                                                <td>
                                                    {{ $user->email ? $user->email : 'N/A' }}
                                                </td>
                                                <td>
                                                    {{ $user->phone_no ? $user->phone_no : 'N/A' }}
                                                </td>
                                                <td>
                                                    {{ $user->deleted_at ? 'De-Activated' : 'Active' }}
                                                </td>
                                                <td class="text-right">
                                                    <button class="btn btn-primary"><spna class="fas fa-eye"></spna></button>
                                                    <button class="btn btn-success"><spna class="fas fa-pencil-alt"></spna></button>
                                                    <button class="btn btn-danger"><spna class="far fa-trash-alt"></spna></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footer')
    </div>
</div>
<!--   Core JS Files   -->
<script src="./js/core/jquery.min.js"></script>
<script src="./js/core/popper.min.js"></script>
<script src="./js/core/bootstrap.min.js"></script>
<script src="./js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!-- Chart JS -->
<script src="./js/plugins/chartjs.min.js"></script>
<!--  Notifications Plugin    -->
<script src="./js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
<script src="./js/now-ui-dashboard.min.js?v=1.1.0" type="text/javascript"></script>
<!-- Now Ui Dashboard DEMO methods, don't include it in your project! -->
<script src="./demo/demo.js"></script>


<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js"></script>

<script>
    ( function($){

        $(document).ready(function() {
            $('#user_table').DataTable();
        } );

    }) (jQuery);

    (function($){
        $(document).ready(function(){

            // var search_box = '<div class="input-group no-border">'+
            //                 '<input type="search" value="" class="form-control" placeholder="Search..." aria-controls="user_table">'+
            //                 '<div class="input-group-append">'+
            //                 '<div class="input-group-text">'+
            //                 '<i class="now-ui-icons ui-1_zoom-bold"></i>'+
            //                 '</div>'+
            //                 '</div>'+
            //                 '</div>';
            //
            // $('#user_table_filter').html(search_box);

            $('input[type=search]').removeClass('form-control-sm');
            // $('input[type=search]').attr("placeholder", "Search");


        });
    })(jQuery);
</script>
</body>

</html>