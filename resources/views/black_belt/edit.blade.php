
<!DOCTYPE html>
<html lang="en">

<head>
    @if(Auth::user()->pro == 'yes')
        @include('layouts.pro.head')
        <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    @else
        @include('layouts.head')
        <script src="{{ asset('js/sweetalert.min.js') }}"></script>
        <link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('js/font-awesome.min.js') }}" />
    @endif
</head>
<style>
    .form-group input[type=file] {
        opacity: unset;
        position: inherit;
    }
</style>
<body class="sidebar-mini">
<div class="wrapper ">
    @if(Auth::user()->pro == 'yes')
        @include('layouts.pro.side-nav')
    @else
        @include('layouts.side-nav')
    @endif
    <div class="main-panel">
        <!-- Navbar -->
    @if(Auth::user()->pro == 'yes')
        @include('layouts.pro.nav')
    @else
        @include('layouts.nav')
    @endif
    <!-- End Navbar -->
        <div class="panel-header panel-header-sm">
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="title">Edit</h5>
                        </div>
                        <form class="form-horizontal" method="POST" action="{{ route('black-belt.update',['id' => $user->id]) }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 pr-1">
                                        <div class="form-group">
                                            <label>Full Name</label>
                                            {{--                                        --}}{{--                                            <input type="label" class="form-control" disabled="" placeholder="Company" value="Creative Code Inc.">--}}
                                            <input id="fullname" type="text" class="form-control" name="fullname" value="{{ $user->full_name }}" required>

                                            @if ($errors->has('fullname'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('fullname') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6 pl-1">
                                        <div class="form-group">
                                            <label>Dan</label>
                                            <select class="form-control" id="dan" name="dan">
                                                <option value="">--- SELECT ---</option>
                                                @foreach($dan_list as $value)
                                                    @if(!empty($value))
                                                        <option value="{{ $value->id }}" {{ $value->id == $user->dan_list_id ? 'selected' : '' }}>{{ $value->dan }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 pr-1">
                                        <div class="form-group">
                                            <label>Year</label>
                                            <select class="form-control" id="year" name="year">
                                                <option value="">--- SELECT ---</option>
                                                @foreach($year as $y)
                                                    <option value="{{ $y }}" {{ $y == $user->year ? 'selected' : '' }}>{{ $y }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pl-1">
                                        <div class="form-group">
                                            <label>Instructor</label>
                                            <select class="form-control" id="instructor" name="instructor" required>
                                                <option value="">--- SELECT ---</option>
                                                <option value="yes" {{ $user->instructor == 'yes' ? 'selected' : '' }}>Yes</option>
                                                <option value="no" {{ $user->instructor == 'no' ? 'selected' : '' }}>No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 pr-1">
                                        <div class="form-group">
                                            <label>Image</label>
                                            <input id="name" type="file" class="form-control" name="name"  />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="card-body">
                                <center>
                                    <button class="btn btn-primary">Submit</button>
                                </center>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-user">
                        <div class="image">
                            <img src="{{ asset('img/bg5.jpg') }}" alt="...">
                        </div>
                        <div class="card-body">
                            <div class="author">
                                <a href="#">
                                    <img class="avatar border-gray" src="{{ !empty($user->image_url) ? asset($user->image_url) : asset('img/mike.jpg') }}" alt="{{$user->full_name}}">
                                    <h5 class="title">{{ $user->full_name ? $user->full_name : ''  }}</h5>
                                </a>
                                <p class="description">
                                    {{ $user->dan_list->dan ? $user->dan_list->dan : '' }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footer')
    </div>
</div>
@if(Auth::user()->pro == 'yes')
    @include('layouts.pro.fixed-plugin')
    @include('layouts.pro.script')
@else
    @include('layouts.script')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js"></script>
@endif
</body>

</html>