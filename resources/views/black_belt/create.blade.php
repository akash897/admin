<!DOCTYPE html>
<html lang="en">

<head>
    @if(Auth::user()->pro == 'yes')
        @include('layouts.pro.head')
        <script src="{{ asset('js/sweetalert.min.js') }}"></script>
    @else
        @include('layouts.head')
        <script src="{{ asset('js/sweetalert.min.js') }}"></script>
        <link href="{{ asset('css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('js/font-awesome.min.js') }}" />
    @endif
</head>
<style>
    .form-group input[type=file] {
        opacity: unset;
        position: inherit;
    }
</style>
<body class="sidebar-mini">
<div class="wrapper ">
    @if(Auth::user()->pro == 'yes')
        @include('layouts.pro.side-nav')
    @else
        @include('layouts.side-nav')
    @endif
    <div class="main-panel">
        <!-- Navbar -->
    @if(Auth::user()->pro == 'yes')
        @include('layouts.pro.nav')
    @else
        @include('layouts.nav')
    @endif
    <!-- End Navbar -->
        <div class="panel-header panel-header-sm">
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="title">Add Data</h5>
                        </div>
                        <form class="form-horizontal" method="POST" action="{{ route('black-belt.store') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 pr-1">
                                        <div class="form-group">
                                            <label>Full Name</label>
                                            <input id="fullname" type="text" class="form-control" name="fullname"  required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pl-1">
                                        <div class="form-group">
                                            <label>Dan</label>
                                            <select class="form-control" id="dan" name="dan">
                                                <option value="">--- SELECT ---</option>
                                                @foreach($dan_list as $value)
                                                    @if(!empty($value))
                                                        <option value="{{ $value->id }}" >{{ $value->dan }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 pr-1">
                                        <div class="form-group">
                                            <label>Year</label>
                                            <select class="form-control" id="year" name="year">
                                                <option value="">--- SELECT ---</option>
                                                @foreach($year as $y)
                                                    <option value="{{ $y }}" >{{ $y }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 pl-1">
                                        <div class="form-group">
                                            <label>Image</label>
                                            <input id="name" type="file" class="form-control" name="name"  />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="card-body">
                                <center>
                                    <button class="btn btn-primary">Submit</button>
                                </center>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footer')
    </div>
</div>
@if(Auth::user()->pro == 'yes')
    @include('layouts.pro.fixed-plugin')
    @include('layouts.pro.script')
@else
    @include('layouts.script')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js"></script>
@endif
</body>

</html>