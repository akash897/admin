<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DanList extends Model
{
    public function black_belt()
    {
        return $this->hasOne('App\BlackBelt');
    }
}
