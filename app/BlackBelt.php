<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlackBelt extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function dan_list()
    {
        return $this->belongsTo('App\DanList');
    }

    public  function instructor_detail()
    {
        return $this->hasOne('App\Instructor');
    }
}
