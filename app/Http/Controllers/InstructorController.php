<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlackBelt;

class InstructorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_name = 'Instructor List';
        $values = BlackBelt::withTrashed()
                            ->where('instructor','yes')
                            ->get();
        return view('instructor.index',compact('values','page_name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page_name = 'Instructor';
        $user = BlackBelt::withTrashed()
                ->where('id', $id)
                ->get();
        $value = $user[0] ? $user[0] : null;
        $instructor = $value->instructor_detail;
        return view('instructor.view',compact('value', 'page_name','instructor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $black_belt_user = BlackBelt::findorfail($id);
        if(!empty($black_belt_user)){
            $black_belt_user->delete();
            $result = [
                'response' => 'success',
                'code' => '200'
            ];
        }
        else{
            $result = [
                'response' => 'error',
                'code' => '404'
            ];
        }

        return response()->json($result);
    }
}
