<?php

namespace App\Http\Controllers;

use App\User;
use App\UserSetting;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent;
use phpDocumentor\Reflection\Types\Array_;

class UserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_name = 'User List';
        $users = User::withTrashed()->get();
        return view('user.index',compact('users','page_name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

//        dd($this->guard()->user()->role);
        $user = Auth::user();
        $user->api_token = str_random(64);
        $user->save();
        return redirect('/profile');

//        if($this->guard()->user()->role->key == 'super_admin')
//            return redirect('admin');
//        else
//            return redirect('admin/profile');
    }

    public function logout(Request $request)
    {
        $user = Auth::user();
        $user->api_token = null;
        $user->save();

        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }

    public function profile(){

        $page_name = 'User Profile';
        $user = Auth::user();
        $agent = new Agent();
        $agent_data = new Array_();
        $agent_data->is_mobile = $agent->isMobile();
        $agent_data->is_tablet = $agent->isTablet();
        $agent_data->languages = $agent->languages();
        $agent_data->device = $agent->device();
        $agent_data->is_desktop = $agent->isDesktop();
        $agent_data->is_iphone = $agent->isPhone();
        $agent_data->is_robot = $agent->isRobot();
        $agent_data->robot_name = $agent->robot();
        $agent_data->browser = $agent->browser();
        $agent_data->browser_version = $agent->version($agent->browser());
        $agent_data->platform = $agent->platform();
        $agent_data->platform_version = $agent->version($agent->platform());
//        dd($agent_data);
        return view('admin.index',compact('user','agent_data','page_name'));
    }

    public function dashboard(){
        $page_name = 'Dashboard';
        return view('admin.dashboard', compact('page_name'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function color_change(Request $request){
        $user = Auth::user();
        $user_setting = $user->user_setting;
        $user_setting->color_name = $request->color;
        $user_setting->save();
        $result = [
            'response' => 'success, color change',
            'code' => '200'
        ];

        return response()->json($result);
    }
}
