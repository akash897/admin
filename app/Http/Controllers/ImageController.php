<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image;
use App\Image as Images;


class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Request $request
     */
    public function UploadImage(Request $request){
        if($request->hasFile('file')) {
            //get filename with extension
            $filenamewithextension = $request->file('file')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('file')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename.'.'.$extension;

            $path_of_original_image = 'public/'.$request->album_name;
            $path_of_thumbnail_image = 'public/'.$request->album_name.'/thumbnail';
            $storage_img_path = 'storage/'.$request->album_name.'/';
            $storage_thumb_path = 'storage/'.$request->album_name.'/thumbnail/';

            //Upload File
            $request->file('file')->storeAs($path_of_original_image, $filenametostore);
            $request->file('file')->storeAs($path_of_thumbnail_image, $filenametostore);

            //Resize image here
            $imgpath = public_path($storage_img_path.$filenametostore);
            $this->ImageConversion($imgpath);
            $thumbnailpath = public_path($storage_thumb_path.$filenametostore);
            $this->ImageThumbnailConversion($thumbnailpath);
            $return_path = '/upload/'.$request->album_name;

            $image_url = $path_of_original_image.'/'.$filenametostore;
            $thumbnail_url = $path_of_thumbnail_image.'/'.$filenametostore;

            $image = new Images();
            $image->album_id = $request->album_id;
            $image->image_url = $image_url;
            $image->thumbnail_url = $thumbnail_url;
            $image->save();

            return redirect($return_path)->with('success', "Image uploaded successfully.");
        }
        else
            return 'false';
    }

    /**
     * @param $event
     * @param $album
     */
    public function dropzoneView($album){
        return view('dropzone.dropzone', compact('album'));
    }

    public function ImageConversion($imgpath){
        $img = Image::make($imgpath)->resize(2000, 2000, function($constraint) {
            $constraint->aspectRatio();
        });
        $img->save($imgpath);
    }

    public function ImageThumbnailConversion($thumbnailpath){
        $thumb_img = Image::make($thumbnailpath)->resize(400, 400, function($constraint) {
            $constraint->aspectRatio();
        });
        $thumb_img->crop(400, 400);
        $thumb_img->save($thumbnailpath);
    }
}
