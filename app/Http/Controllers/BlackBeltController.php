<?php

namespace App\Http\Controllers;

use App\DanList;
use App\Instructor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\BlackBelt;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class BlackBeltController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_name = 'Black Belt List';
        $values = BlackBelt::withTrashed()
                            ->where('instructor','no')
                            ->get();
        return view('black_belt.index',compact('values','page_name'));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $page_name = 'Create';
        $dan_list = DanList::all();
        $year = [];
        $current_year = Carbon::now()->year;
        for($i = $current_year ; $i >= 1950 ; $i-- ){
            $year[] = $i;
        }
        return view('black_belt.create',compact('year','page_name','dan_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $blackbelt = new BlackBelt();
        $blackbelt->full_name = $request->fullname;
        $blackbelt->dan_list_id = $request->dan;
        $blackbelt->year = $request->year;

        if(Input::hasFile('name')){

            $file = Input::file('name');
            $file->move('uploads/instructors', $file->getClientOriginalName());
            $blackbelt->image_url = 'uploads/instructors/'.$file->getClientOriginalName();
        }

        $blackbelt->created_by = Auth::user()->name;
        $blackbelt->modified_by = Auth::user()->name;
        $blackbelt->save();
        return redirect()->route('black-belt.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page_name = 'Black Belt User';
        $user = BlackBelt::withTrashed()
                ->where('id', $id)
                ->get();
        $value = $user[0] ? $user[0] : null;
        return view('black_belt.view',compact('value', 'page_name'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_name = 'Black Belt User Edit';
        $user = BlackBelt::findorfail($id);
        $dan_list = DanList::all();
        $year = [];
        $current_year = Carbon::now()->year;
        for($i = $current_year ; $i >= 1950 ; $i-- ){
            $year[] = $i;
        }
        return view('black_belt.edit',compact('user','dan_list','year','page_name'));
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|null
     */
    public function update(Request $request, $id)
    {
        $user = BlackBelt::findorfail($id);

        if(!empty($user)){

            $instructor = Instructor::where('black_belt_id',$id)->get();

            if(empty($instructor[0])){
                $new_instructor = new Instructor();
                $new_instructor->black_belt_id = $user->id;
                $new_instructor->save();
            }

            $user->full_name = $request->fullname;
            $user->dan_list_id = $request->dan;
            $user->instructor = $request->instructor;
            $user->year = $request->year;

            if(Input::hasFile('name')){

                if(!empty($user->image)){
                    $pathOfImage = public_path().'/uploads/instructors/'.$user->image_url;
                    File::delete($pathOfImage);
                }

                $file = Input::file('name');
                $file->move('uploads/instructors', $file->getClientOriginalName());
                $user->image_url = 'uploads/instructors/'.$file->getClientOriginalName();
            }

            $user->modified_by = Auth::user()->name;

            $user->save();

            return redirect()->route('black-belt.index');
        }else{
            return null;
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $black_belt_user = BlackBelt::findorfail($id);
        if(!empty($black_belt_user)){
            $black_belt_user->delete();
            $result = [
                'response' => 'success',
                'code' => '200'
            ];
        }
        else{
            $result = [
                'response' => 'error',
                'code' => '404'
            ];
        }

        return response()->json($result);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function recovery($id){
        $blackbelt = BlackBelt::withTrashed()->findorfail($id);
        if($blackbelt->deleted_at != null){
            $blackbelt->deleted_at = null;
            $blackbelt->save();
            return redirect()->route('black-belt.view',['id'=>$id]);
        }
        else
            return redirect()->route('black-belt.index');
    }
}
