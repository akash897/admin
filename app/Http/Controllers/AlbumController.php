<?php

namespace App\Http\Controllers;

use App\Album;
use App\Event;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page_name = 'Album';
        $values = Album::all();
        return view('album.index',compact('values','page_name'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_name = 'Create Album';
        $events = Event::all();
        return view('album.create',compact('page_name','events'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $album = new Album();
        $album->event_id = $request->event;
        $album->name = $request->album_name;
        $album->key = str_replace(' ','-',strtolower($request->album_name));
        if(Input::hasFile('name')){

            $file = Input::file('name');
            $file->move('uploads/albums', $file->getClientOriginalName());
            $album->image_url = 'uploads/albums/'.$file->getClientOriginalName();
        }
        $album->save();
        return redirect()->route('album.index');
    }

    /**
     * @param $album
     */
    public function show($album)
    {
        $page_name = 'Upload Photos';
        $album_data = Album::where('key',$album)->get();
        $images = Image::where('album_id',$album_data[0]->id)
                        ->orderBy('id')
                        ->get();
        $album_id = $album_data[0]->id;
        $album_name = $album_data[0]->key;
        return view('album.upload',compact('page_name','album_id','album_name','images'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
