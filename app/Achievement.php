<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Achievement extends Model
{
    public function instructor_detail()
    {
        return $this->belongsTo('App\Instructor');
    }
}
