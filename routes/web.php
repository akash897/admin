<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/login',function (){
    return view('admin.login');
})->name('login');
Route::post('/login', 'UserController@login')->name('auth.login');
//Route::get('/password/reset', 'UserPasswordResetController@reset')->name('password.request');
//Route::post('/logout', 'UserController@logout')->name('logout');
Route::get('/home', 'HomeController@index')->name('home');

Route::group([ 'middleware' => 'auth'], function()
{
    Route::get('/', 'UserController@dashboard')->name('dashboard');


    Route::get('/profile', 'UserController@profile')->name('profile');


    Route::get('/dashboard', 'UserController@dashboard')->name('dashboard');


    Route::get('/user', 'UserController@index')->name('user.index');

    Route::get('/black-belt', 'BlackBeltController@index')->name('black-belt.index');
    Route::get('/black-belt/create', 'BlackBeltController@create')->name('black-belt.create');
    Route::post('/black-belt/create', 'BlackBeltController@store')->name('black-belt.store');
    Route::get('/black-belt/{id}', 'BlackBeltController@show')->name('black-belt.view');
    Route::get('/black-belt/{id}/edit', 'BlackBeltController@edit')->name('black-belt.edit');
    Route::post('/black-belt/{id}', 'BlackBeltController@update')->name('black-belt.update');
    Route::delete('/black-belt/{id}', 'BlackBeltController@destroy')->name('black-belt.delete');
    Route::get('/black-belt/{id}/recovery', 'BlackBeltController@recovery')->name('black-belt.recovery');

    Route::get('/instructor', 'InstructorController@index')->name('instructor.index');
    Route::get('/instructor/{id}', 'InstructorController@show')->name('instructor.view');
    Route::get('/instructor/{id}/edit', 'InstructorController@edit')->name('instructor.edit');
    Route::delete('/instructor/{id}', 'InstructorController@destroy')->name('instructor.delete');

    Route::get('/event', 'EventController@index')->name('event.index');
    Route::get('/event/create', 'EventController@create')->name('event.create');
    Route::post('/event/create', 'EventController@store')->name('event.store');

    Route::get('/album', 'AlbumController@index')->name('album.index');
    Route::get('/album/create', 'AlbumController@create')->name('album.create');
    Route::post('/album/create', 'AlbumController@store')->name('album.store');
    Route::get('/album/{album}', 'AlbumController@show')->name('album.show');

    Route::get('/upload/{album}', 'ImageController@dropzoneView')->name('dropzone');

    Route::post('/upload_file', 'ImageController@UploadImage')->name('image_post');



    Route::post('/user-setting/color/{id}', 'UserController@color_change')->name('color_change');


});